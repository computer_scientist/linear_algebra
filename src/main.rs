use linear_algebra::{dot_product, multiply_vector};

fn main() {
    let vector: [f64; 2] = [3.0, -1.0];
    let scalar: f64 = 2.3;
    // This demonstrates f64 being about 15 places precise.
    let expected: Vec<f64> = vec![6.8999999999999995, -2.3]; // for testing

    // For testing
    let result: Vec<f64> = multiply_vector(&vector, &scalar);
    assert_eq!(result, expected);
}
