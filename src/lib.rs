//! # Linear Algebra
//!
//! This library was meant to complete exercises from Mike X Cohen's Linear Algebra
//! course. There are no plot implementation in this project as it was meant
//! to just do the calculations to supplement calculations done by hand.

pub mod vectors;
pub use vectors::arithmetic::*;
pub use vectors::products::*;
