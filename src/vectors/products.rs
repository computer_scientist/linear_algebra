/// Does dot product of two arrays of the same size.
///
/// # Example uses:
///
/// ```rust
/// use linear_algebra::dot_product;
///
/// let v1: [f64; 5] = [1.0, 2.0, 3.0, 4.0, 5.0];
/// let v2: [f64; 5] = [0.0, -4.0, -3.0, 6.0, 5.0];
/// let expected: f64 = 32.0; // For testing
///
/// // Use Case 1:
/// match dot_product(&v1, &v2) {
///     Ok(scalar) => println!("v1 dot v2: {}", scalar),
///     Err(e) => eprint!("Error: {}", e),
/// };
///
/// // Use Case 2:
/// let product: f64 = dot_product(&v1, &v2).unwrap_or_else(|err| {
///     eprintln!("Error occurred when calculating dot product: {}", err);
///     0.0 // If error occurs, this is assigned as default.
/// });
///
/// // For Testing:
/// match dot_product(&v1, &v2) {
///     Ok(result) => assert_eq!(result, expected),
///     Err(e) => panic!("Test failed with error: {}", e),
/// };
/// ```
pub fn dot_product(v1: &[f64], v2: &[f64]) -> Result<f64, &'static str> {
    if v1.len() != v2.len() {
        return Err("Vectors need to be of the same length.");
    }

    Ok(v1
        .iter()
        .zip(v2.iter())
        .map(|(&v1_axis, &v2_axis)| v1_axis * v2_axis)
        .sum())
}
