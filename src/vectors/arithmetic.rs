/// Does vector addition of two arrays of size N.
///
/// # Example uses:
///
/// ```rust
/// use linear_algebra::add_vectors;
///
/// let v1: [f64; 2] = [3.0, -1.0];
/// let v2: [f64; 2] = [2.0, 4.0];
/// let expected: [f64; 2] = [5.0, 3.0]; // Variable for testing
///
/// // Use Case 1:
/// match add_vectors(&v1, &v2) {
///     Ok(v3) => println!("v3: {:?}", v3),
///     Err(e) => eprintln!("Error: {}", e),
/// }
///
/// // Use Case 2:
/// let v3: Vec<f64> = add_vectors(&v1, &v2).unwrap_or_else(|err| {
///     eprintln!("Error occurred when adding vectors: {}", err);
///     vec![] // If error occurs, this is assigned as default (empty vec).
/// });
///
/// // For testing
/// match add_vectors(&v1, &v2) {
///     Ok(result) => assert_eq!(result, expected),
///     Err(e) => panic!("Test failed with error: {}", e),
/// }
/// ```
pub fn add_vectors(v1: &[f64], v2: &[f64]) -> Result<Vec<f64>, &'static str> {
    if v1.len() != v2.len() {
        return Err("Vectors must be of the same length.");
    }

    Ok(v1
        .iter()
        .zip(v2.iter())
        .map(|(&v1_axis, &v2_axis)| v1_axis + &v2_axis)
        .collect())
}

/// Does vector subtraction of two arrays of size N.
///
/// # Example uses:
///
/// ```rust
/// use linear_algebra::subtract_vectors;
///
/// let v1: [f64; 2] = [3.0, -1.0];
/// let v2: [f64; 2] = [2.0, 4.0];
/// let expected: [f64; 2] = [1.0, -5.0]; // Variable for testing
///
/// // Use Case 1:
/// match subtract_vectors(&v1, &v2) {
///     Ok(v3) => println!("v3: {:?}", v3),
///     Err(e) => eprintln!("Error: {}", e),
/// }
///
/// // Use Case 2:
/// let v3: Vec<f64> = subtract_vectors(&v1, &v2).unwrap_or_else(|err| {
///     eprintln!("Error occurred when subtracting vectors: {}", err);
///     vec![] // If error occurs, this is assigned as default (empty vec).
/// });
///
/// // For testing
/// match subtract_vectors(&v1, &v2) {
///     Ok(result) => assert_eq!(result, expected),
///     Err(e) => panic!("Test failed with error: {}", e),
/// }
/// ```
pub fn subtract_vectors(v1: &[f64], v2: &[f64]) -> Result<Vec<f64>, &'static str> {
    if v1.len() != v2.len() {
        return Err("Vectors must be of the same length.");
    }

    Ok(v1
        .iter()
        .zip(v2.iter())
        .map(|(&v1_axis, &v2_axis)| v1_axis - v2_axis)
        .collect())
}

/// Does vector scalar multiplication of an array of size N with a scalar.
///
/// # Example Uses:
/// ```
/// use linear_algebra::multiply_vector;
///
/// let vector: [f64; 2] = [3.0, -1.0];
/// let scalar: f64 = 2.3;
///
/// // This demonstrates f64 being about 15 places precise.
/// let expected: Vec<f64> = vec![6.8999999999999995, -2.3]; // for testing
///
/// let new_vector: Vec<f64> = multiply_vector(&vector, &scalar);
///
/// assert_eq!(new_vector, expected);
/// ```
pub fn multiply_vector(vector: &[f64], lambda: &f64) -> Vec<f64> {
    vector
        .iter()
        .map(|&vector_axis| vector_axis * lambda)
        .collect()
}
